#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <ranges>
#include <algorithm>

using namespace std;

std::ostream& operator<<(std::ostream &os, const std::vector<int> v){
    for(auto el: v){
        os << setw(2) << el << ", ";
    }
    return os << '\n';
}

std::ostream& operator<<(std::ostream &os, const std::vector<long long> v){
    for(auto el: v){
        os << setw(2) << el << ", ";
    }
    return os << '\n';
}

vector<int> getInput(const char *filename){
	vector<int> retval{0};
	ifstream input(filename);
	int line;
	/* copy(input.begin(), input.end(), inserter(retval, end(retval)); */
	while( input >> line ){
		retval.push_back(line);
	}
	return retval;
}

auto paths(const vector<int> &adapters){
    vector<long long> path_memo(adapters.size(),0);
    path_memo.at(path_memo.size()-1) = 1;
    for(int idx1 = path_memo.size() - 1; idx1 >= 0; --idx1){
        size_t idx2 = idx1;
        while(++idx2 < adapters.size() && adapters[idx2] - adapters[idx1] <= 3 ){
            path_memo[idx1] += path_memo[idx2];
        }
    }
    return path_memo;
}

int main(int argc, char *argv[])
{
	vector<int> adapters{ getInput(argv[1]) };
	ranges::sort(adapters);
	adapters.push_back(adapters.back() + 3);
	vector<int> difference{}; 
	difference.reserve(adapters.size() - 1);
	std::ranges::transform(
			adapters.begin(), adapters.end() - 1, 
			adapters.begin() + 1, adapters.end(),
			back_inserter(difference), [](int i, int j){ return j - i; } );
	auto count1 = ranges::count(difference, 1);
	auto count3 =	ranges::count(difference, 3);
	cout << count1 * count3 << '\n';

	std::cout << paths(adapters) << '\n';

	return 0;
}
